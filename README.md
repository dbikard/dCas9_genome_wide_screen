This repository contains the code used in the paper:

***Genome-wide CRISPR-dCas9 screens in E. coli identify essential genes and phage host factors***,

François Rousset, Lun Cui, Elise Siouve, Florence Depardieu & David Bikard (2018)

In this study we use a library of guide RNAs targeting random positions
along the genome of E. coli MG1655, with the simple requirement of a “NGG” PAM. After transformation into cells expressing dCas9 under control of an aTc-inducible promoter, the library of cells can be used to perform high-throughput genetic screens.
In the essentiality screen, the pooled library of cells was grown in rich medium over 17 generations with anhydrotetracycline (aTc). The fold change in abundance (log2FC) of the guide RNA was measured through deep sequencing of the library. In the phage screens, cells were infected by either lambda, T4 or 186 for 2h. Sequencing was performed before and after infection to compare the enrichment and depletion of guides during the infection.


The analysis is divided in three jupyter notebooks:

* Generating count tables from fastq files.ipynb
> In this notebook we generate count tables for both screens from the raw sequencing data available on the European Nucleotide Archive with the accession number PRJEB28256 (Python).


* essentiality screen analysis.ipynb
> In this notebook we perform statistical analysis of the essentiality screen data (R).


* phage screens analysis.ipynb
> In this notebook we perform statistical analysis of the phage screens data (R).
